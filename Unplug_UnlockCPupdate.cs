﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Unified_Client
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
	/// <summary>
	///The Unplug_UnlockCPupdate recording.
	/// </summary>
	[TestModule("16c3a563-f366-43f3-8703-6583fd429c4a", ModuleType.Recording, 1)]
	public partial class Unplug_UnlockCPupdate : ITestModule
	{
		/// <summary>
		/// Holds an instance of the Unified_ClientRepository repository.
		/// </summary>
		public static Unified_ClientRepository repo = Unified_ClientRepository.Instance;

		static Unplug_UnlockCPupdate instance = new Unplug_UnlockCPupdate();

		/// <summary>
		/// Constructs a new instance.
		/// </summary>
		public Unplug_UnlockCPupdate()
		{
		}

		/// <summary>
		/// Gets a static instance of this recording.
		/// </summary>
		public static Unplug_UnlockCPupdate Instance
		{
			get { return instance; }
		}

#region Variables

#endregion

		/// <summary>
		/// Starts the replay of the static recording <see cref="Instance"/>.
		/// </summary>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
		public static void Start()
		{
			TestModuleRunner.Run(Instance);
		}

		/// <summary>
		/// Performs the playback of actions in this recording.
		/// </summary>
		/// <remarks>You should not call this method directly, instead pass the module
		/// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
		/// that will in turn invoke this method.</remarks>
		[System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
		void ITestModule.Run()
		{
			Mouse.DefaultMoveTime = 0;
			Keyboard.DefaultKeyPressTime = 20;
			Delay.SpeedFactor = 0.00;

			Init();

			Report.Log(ReportLevel.Info, "Application", "Run application 'C:\\Users\\kcochran\\Downloads\\brainstem_dev_kit_mswindows_i386_23\\bin\\HubTool.exe' in normal mode.", new RecordItemIndex(0));
			Host.Local.RunApplication("C:\\Users\\kcochran\\Downloads\\brainstem_dev_kit_mswindows_i386_23\\bin\\HubTool.exe", "", "C:\\Users\\kcochran\\Downloads\\brainstem_dev_kit_mswindows_i386_23\\bin", false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'HubToolV288.Power' at Center.", repo.HubToolV288.PowerInfo, new RecordItemIndex(1));
			repo.HubToolV288.Power.Click();
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 10s.", new RecordItemIndex(2));
			Delay.Duration(10000, false);
			
			Report.Log(ReportLevel.Info, "Application", "Run application 'J:\\Unlocker.exe' in normal mode.", new RecordItemIndex(3));
			Host.Local.RunApplication("J:\\Unlocker.exe", "", "J:\\", false);
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 15s.", new RecordItemIndex(4));
			Delay.Duration(15000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.PasswordLineEdit' at 19;10.", repo.ControlPanel.PasswordLineEditInfo, new RecordItemIndex(5));
			repo.ControlPanel.PasswordLineEdit.Click("19;10");
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Datalocker1{RShiftKey down}!{RShiftKey up}' with focus on 'ControlPanel.PasswordLineEdit'.", repo.ControlPanel.PasswordLineEditInfo, new RecordItemIndex(6));
			repo.ControlPanel.PasswordLineEdit.PressKeys("Datalocker1{RShiftKey down}!{RShiftKey up}");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Unlock' at 51;15.", repo.ControlPanel.UnlockInfo, new RecordItemIndex(7));
			repo.ControlPanel.Unlock.Click("51;15");
			
			//Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Settings' at Center.", repo.ControlPanel.SettingsInfo, new RecordItemIndex(8));
			//repo.ControlPanel.Settings.Click();
			
			//Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2' at 58;11.", repo.ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2Info, new RecordItemIndex(9));
			//repo.ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2.Click("58;11");
			
			//Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31' at 46;16.", repo.ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31Info, new RecordItemIndex(10));
			//repo.ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31.Click("46;16");
			
			//Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CheckingForUpdates.UpdateProgressCancelButton' at 142;14.", repo.CheckingForUpdates.UpdateProgressCancelButtonInfo, new RecordItemIndex(11));
			//repo.CheckingForUpdates.UpdateProgressCancelButton.Click("142;14");
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 10s.", new RecordItemIndex(12));
			Delay.Duration(10000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Lock' at 72;15.", repo.ControlPanel.LockInfo, new RecordItemIndex(13));
			repo.ControlPanel.Lock.Click("72;15");
			
			try {
				Report.Log(ReportLevel.Info, "Wait", "(Optional Action)\r\nWaiting 300ms to exist. Associated repository item: 'Warning.YourDevicesAntiMalwareIsCurrentlyB'", repo.Warning.YourDevicesAntiMalwareIsCurrentlyBInfo, new ActionTimeout(300), new RecordItemIndex(14));
				repo.Warning.YourDevicesAntiMalwareIsCurrentlyBInfo.WaitForExists(300);
			} catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(14)); }
			
			try {
				Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'Warning.ButtonOK1' at Center.", repo.Warning.ButtonOK1Info, new RecordItemIndex(15));
				repo.Warning.ButtonOK1.Click();
			} catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(15)); }
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(16));
			Delay.Duration(5000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'HubToolV288.Power' at Center.", repo.HubToolV288.PowerInfo, new RecordItemIndex(17));
			repo.HubToolV288.Power.Click();
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(18));
			Delay.Duration(5000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'HubToolV288.Power' at Center.", repo.HubToolV288.PowerInfo, new RecordItemIndex(19));
			repo.HubToolV288.Power.Click();
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(20));
			Delay.Duration(5000, false);
			
			Report.Log(ReportLevel.Info, "Application", "Run application 'J:\\Unlocker.exe' in normal mode.", new RecordItemIndex(21));
			Host.Local.RunApplication("J:\\Unlocker.exe", "", "J:\\", false);
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 15s.", new RecordItemIndex(22));
			Delay.Duration(15000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'ControlPanel.PasswordLineEdit' at 19;10.", repo.ControlPanel.PasswordLineEditInfo, new RecordItemIndex(23));
			repo.ControlPanel.PasswordLineEdit.DoubleClick("19;10");
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Datalocker1{RShiftKey down}!{RShiftKey up}' with focus on 'ControlPanel.PasswordLineEdit'.", repo.ControlPanel.PasswordLineEditInfo, new RecordItemIndex(24));
			repo.ControlPanel.PasswordLineEdit.PressKeys("Datalocker1{RShiftKey down}!{RShiftKey up}");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Unlock' at 51;15.", repo.ControlPanel.UnlockInfo, new RecordItemIndex(25));
			repo.ControlPanel.Unlock.Click("51;15");
			
			Report.Log(ReportLevel.Info, "Website", "Opening web site 'https://nightly.safeconsolecloud.com/safeconsole/#/login' with browser 'Chrome' in maximized mode.", new RecordItemIndex(26));
			Host.Current.OpenBrowser("https://nightly.safeconsolecloud.com/safeconsole/#/login", "Chrome", "", false, true, false, false, false, false, false, true);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.UseSingleSignOnSSO' at 129;7.", repo.SafeConsole.UseSingleSignOnSSOInfo, new RecordItemIndex(27));
			repo.SafeConsole.UseSingleSignOnSSO.Click("129;7");
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(28));
			Delay.Duration(5000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SafeConsole.ContentNgScope.JavascriptVoid0' at 112;14.", repo.SafeConsole.ContentNgScope.JavascriptVoid0Info, new RecordItemIndex(29));
			repo.SafeConsole.ContentNgScope.JavascriptVoid0.DoubleClick("112;14");
			
			//Report.Log(ReportLevel.Info, "Wait", "Waiting 5s to exist. Associated repository item: 'SafeConsole.ContentNgScope.PortletBody'", repo.SafeConsole.ContentNgScope.PortletBodyInfo, new ActionTimeout(5000), new RecordItemIndex(30));
			//repo.SafeConsole.ContentNgScope.PortletBodyInfo.WaitForExists(5000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Text' at 83;23.", repo.TextInfo, new RecordItemIndex(31));
			repo.Text.Click("83;23");
			
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '2aff' with focus on 'Text'.", repo.TextInfo, new RecordItemIndex(32));
			repo.Text.PressKeys("2aff");
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(33));
			Delay.Duration(300, false);
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s to exist. Associated repository item: 'SafeConsole.ContentNgScope.DeviceSerialText'", repo.SafeConsole.ContentNgScope.DeviceSerialTextInfo, new ActionTimeout(10000), new RecordItemIndex(34));
			repo.SafeConsole.ContentNgScope.DeviceSerialTextInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Delay", "Waiting for 10s.", new RecordItemIndex(35));
			Delay.Duration(10000, false);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SafeConsole.ContentNgScope.ATag10C37B1CE1F1B3C180002AFF' at 140;5.", repo.SafeConsole.ContentNgScope.ATag10C37B1CE1F1B3C180002AFFInfo, new RecordItemIndex(36));
			repo.SafeConsole.ContentNgScope.ATag10C37B1CE1F1B3C180002AFF.DoubleClick("140;5");
			
			Report.Log(ReportLevel.Info, "Wait", "Waiting 10s to exist. Associated repository item: 'SafeConsole.ModalContent'", repo.SafeConsole.ModalContentInfo, new ActionTimeout(10000), new RecordItemIndex(37));
			repo.SafeConsole.ModalContentInfo.WaitForExists(10000);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.Custom' at 40;10.", repo.SafeConsole.CustomInfo, new RecordItemIndex(38));
			repo.SafeConsole.Custom.Click("40;10");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.SpanTagModifyCustomDevicePolicy' at 176;6.", repo.SafeConsole.SpanTagModifyCustomDevicePolicyInfo, new RecordItemIndex(39));
			repo.SafeConsole.SpanTagModifyCustomDevicePolicy.Click("176;6");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical by -600 units.", new RecordItemIndex(40));
			Mouse.ScrollWheel(-600);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.AntiMalware' at 63;9.", repo.SafeConsole.AntiMalwareInfo, new RecordItemIndex(41));
			repo.SafeConsole.AntiMalware.Click("63;9");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse scroll Vertical by 480 units.", new RecordItemIndex(42));
			Mouse.ScrollWheel(480);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.RestrictDeviceLoginUntilAntiMalware' at -10;7.", repo.SafeConsole.RestrictDeviceLoginUntilAntiMalwareInfo, new RecordItemIndex(43));
			repo.SafeConsole.RestrictDeviceLoginUntilAntiMalware.Click("-10;7");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.UsesavePolicyButton' at 47;12.", repo.SafeConsole.UsesavePolicyButtonInfo, new RecordItemIndex(44));
			repo.SafeConsole.UsesavePolicyButton.Click("47;12");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.ButtonTagOK' at 17;20.", repo.SafeConsole.ButtonTagOKInfo, new RecordItemIndex(45));
			repo.SafeConsole.ButtonTagOK.Click("17;20");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SafeConsole.Close' at 6;3.", repo.SafeConsole.CloseInfo, new RecordItemIndex(46));
			repo.SafeConsole.Close.Click("6;3");
			
			Report.Log(ReportLevel.Info, "Application", "Killing application containing item 'SafeConsoleGoogleChrome'.", repo.SafeConsoleGoogleChrome.SelfInfo, new RecordItemIndex(47));
			Host.Current.KillApplication(repo.SafeConsoleGoogleChrome.Self);
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Settings' at Center.", repo.ControlPanel.SettingsInfo, new RecordItemIndex(48));
			repo.ControlPanel.Settings.Click();
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2' at 58;11.", repo.ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2Info, new RecordItemIndex(49));
			repo.ControlPanel.IKLoginStackedWidgetPageControlPanel10.IKLoginStackedWidgetPageControlPanel2.Click("58;11");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31' at 46;16.", repo.ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31Info, new RecordItemIndex(50));
			repo.ControlPanel.IKLoginStackedWidgetPageControlPanel1.IKLoginStackedWidgetPageControlPanel31.Click("46;16");
			
			try {
				Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating Exists on item 'CheckingForUpdates.UpdateProgressSummaryText'.", repo.CheckingForUpdates.UpdateProgressSummaryTextInfo, new RecordItemIndex(51));
				Validate.Exists(repo.CheckingForUpdates.UpdateProgressSummaryTextInfo, null, false);
			} catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(51)); }
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CheckingForUpdates.UpdateProgressCancelButton' at 142;14.", repo.CheckingForUpdates.UpdateProgressCancelButtonInfo, new RecordItemIndex(52));
			repo.CheckingForUpdates.UpdateProgressCancelButton.Click("142;14");
			
			//Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CheckingForUpdates.UpdateProgressCancelButton' at 142;14.", repo.CheckingForUpdates.UpdateProgressCancelButtonInfo, new RecordItemIndex(53));
			//repo.CheckingForUpdates.UpdateProgressCancelButton.Click("142;14");
			
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ControlPanel.Lock' at 72;15.", repo.ControlPanel.LockInfo, new RecordItemIndex(54));
			repo.ControlPanel.Lock.Click("72;15");
			
			try {
				Report.Log(ReportLevel.Info, "Wait", "(Optional Action)\r\nWaiting 300ms to exist. Associated repository item: 'Warning.YourDevicesAntiMalwareIsCurrentlyB'", repo.Warning.YourDevicesAntiMalwareIsCurrentlyBInfo, new ActionTimeout(300), new RecordItemIndex(55));
				repo.Warning.YourDevicesAntiMalwareIsCurrentlyBInfo.WaitForExists(300);
			} catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(55)); }
			
			try {
				Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'Warning.ButtonOK1' at Center.", repo.Warning.ButtonOK1Info, new RecordItemIndex(56));
				repo.Warning.ButtonOK1.Click();
			} catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(56)); }
			
		}

#region Image Feature Data
#endregion
	}
#pragma warning restore 0436
}
